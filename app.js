'use strict';

// Require Express Web Framework
var express = require('express');
var app = express();
// D3.js for data viz
var d3 = require('d3');






// Declare functions for Routes
var site = require('./site');
var user = require('./user');
var segments = require('./segments');
var athlete = require('./athlete');


// TEMPLATE AND VIEWS CONFIGURATION

// Declare public assets directory
app.use('/assets', express.static(__dirname + '/public'));
// Declare HTML templating engine
app.set('view engine', 'jade');
// Declare template views directory
app.set('views', __dirname + '/views');


/**
 *  INDEX
 */

// Home page route
app.get('/', site.index);


/**
 *  USERS
 */

// Authenticate user
app.get('/login?', user.login);
// Deauthenticate user
app.get('/logout', user.logout);


/**
 *  ATHLETE
 */

// Get authenticated user's recent activities
app.get('/athlete/activities', athlete.activities);
// Get activity by ID
// app.get('/activities/:id?', activity.getActivityById);


/**
 *  SEGMENTS
 */

// Get user's starred segments
app.get('/segments/starred', segments.starred);
// Get segment by ID
app.get('/segments/:id?', segments.getSegmentByID);
// Get a segment's leaderboard
app.get('/segments/:id?/leaderboard', segments.leaderboard);



/**
 *  CREATE WEB SERVER
 */
var ipaddress = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';
var port      = process.env.OPENSHIFT_NODEJS_PORT || 8080;

app.listen(port, ipaddress, function() {
  console.log('Example app listening at http://%s:%s', ipaddress, port);
});
// var server = app.listen(3000, function() {
// 	var host = server.address().address;
// 	var port = server.address().port;
// 	console.log('Example app listening at http://%s:%s', host, port);
// });
