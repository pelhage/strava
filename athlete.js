//GET https://www.strava.com/api/v3/athlete/activities
var strava = require('./requestsHelper');
var config = require('./config');

/**
 * Get a list of activities for authenticated user
 *
 */
exports.activities = function(req, res) {
  if (config.access_token === null) {
    res.redirect('/');
  } else {

    var options = strava.options('athlete/activities');

    var activities = strava.getData(options, res, function(results) {
      res.render('activities', { activities : results });
      // res.send(results);
    });

    activities.on('error', function(e) {
      console.log('problem with request: ' + e.message);
    });
    activities.end();    
  }
};


/*

id: 363738822,
resource_state: 2,
external_id: "3b9350547809425c5747e09d6301d31b",
upload_id: 411125792,
athlete: {
id: 4648488,
resource_state: 1
},
name: "Evening Ride",
distance: 3328.6,
moving_time: 608,
elapsed_time: 771,
total_elevation_gain: 51.6,
type: "Ride",
start_date: "2015-08-08T00:57:50Z",
start_date_local: "2015-08-07T20:57:50Z",
timezone: "(GMT-05:00) America/Detroit",
start_latlng: [
42.58,
-83.32
],
end_latlng: [
42.58,
-83.32
],
location_city: "Bloomfield Hills",
location_state: "MI",
location_country: "United States",
start_latitude: 42.58,
start_longitude: -83.32,
achievement_count: 2,
kudos_count: 0,
comment_count: 0,
athlete_count: 1,
photo_count: 0,
map: {
id: "a363738822",
summary_polyline: "wb|bGv`p{Nm@iAhAmCvFhF`Ybd@vb@t@}`@i@cYmd@eH}FmBdDG~At@@",
resource_state: 2
},
trainer: false,
commute: false,
manual: false,
private: false,
flagged: false,
gear_id: "b2075399",
average_speed: 5.475,
max_speed: 14.5,
average_watts: 128.6,
kilojoules: 78.2,
device_watts: false,
total_photo_count: 0,
has_kudoed: false

*/