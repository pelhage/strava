var https = require('https');
var config = require('./config');


exports.requestData = function(options, res, callback) {
  
  return https.request(options, function(response) {
  
    var body = '';
    
    response.setEncoding('utf8');
    
    response.on('data', function (chunk) {
      body += chunk;
    });
  
    response.on('end', function() {
      var results = JSON.parse(body);
      callback(results);
      res.end();
    });
  });
};



exports.options = function(path) {

  return {
    hostname: 'www.strava.com',
    path: '/api/v3/' + path,
    headers: { 'Authorization' : 'Bearer ' + config.access_token }
  };

};


exports.getData = function(options, res, callback) {
  
  return https.get(options, function(response) {
  
    var body = '';
    
    response.setEncoding('utf8');
    
    response.on('data', function (chunk) {
      body += chunk;
    });
  
    response.on('end', function() {
      var results = JSON.parse(body);
      callback(results);
      res.end();
    });
  });
};