var helper = require('./requestsHelper');
var config = require('./config');
var querystring = require('querystring');


// Setting the Authorization Header to be used by both API calls


exports.starred = function(req, res) {
  if (config.access_token === null) {
    res.redirect('/');
  } else {

    var options = helper.options('segments/starred');

    var segments = helper.getData(options, res, function(results) {
      res.render('segments', { segments : results });
      // res.send(results);
    });

    segments.on('error', function(e) {
      console.log('problem with request: ' + e.message);
    });    
  }
};





/**
 * Get Segments By ID
 *
 */

exports.getSegmentByID = function(req, res) {
  if (config.access_token === null) {
    res.redirect('/');
  } else {
    var segmentID = req.params.id;
    var options = helper.options('segments/' + segmentID);
    
    var segments = helper.getData(options, res, function(results) {
      res.render('segment', { segment : results });
      // res.send(results);
    });

    segments.on('error', function(e) {
      console.log('problem with request: ' + e.message);
    });
  }
};



/**
 *  Get Segment Leaderboard
 *
 */
exports.leaderboard = function(req, res) {
  if (config.access_token === null) {
    res.redirect('/');
  } else {
    var segmentID = req.params.id; // Unique identifier for this segment
    var pageCount = 1; // The current page for the request
    var dataArr; // Consolidate paginated results into a new array
    var lastPlace; // Paginate until reached last place's index

    // Wrap the request so we may paginate it
    function paginatedRequest() {
      var path = 'segments/' + segmentID + 
                 '/leaderboard?page=' + pageCount +
                 '&per_page=200&context_entries=10';
      
      var options = helper.options(path);
      var segments = helper.getData(options, res, function(results) {
        
        // If this is the first request, initialize some vars
        if (pageCount === 1) {
          lastPlace = results.entry_count;
          dataArr = results['entries'];
        } else {
         // Add the retrieved list data to our Array
          dataArr.push(results['entries']);
        }
        // Need to check where we're at to know when to stop paginating.
        var currentLastPlace = results['entries'][results['entries'].length - 1].rank;

        // If this page's last place athlete is NOT dead last...
        if (currentLastPlace < lastPlace) {
          pageCount++;
          paginatedRequest();
        } else {
          res.render('segment-leaderboard', { leaderboard : dataArr });
        }
      });
      
      segments.on('error', function(e) {
        console.log('problem with request: ' + e.message);
      });
    }
    paginatedRequest();
  }
};