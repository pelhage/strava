var config = require('./config');
exports.index = function(req, res) {
	if (config.access_token === null) {
		console.log('Check to see if access token has bene updated..', config.access_token);
  		res.render('index');
	} else {
		res.redirect('/athlete/activities')		
	}
};