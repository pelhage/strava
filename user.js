var querystring = require('querystring');
var helper = require('./requestsHelper');
var config = require('./config');

exports.login = function(req, res) {

  var postData = querystring.stringify({
    'client_id': config.client_id,
    'client_secret': config.client_secret,
    'code': req.query.code
  });

  var options = {
    hostname: 'www.strava.com',
    path: '/oauth/token',
    method: 'POST',
    headers: {
      'Content-Type': 'multipart/form-data',
      'Content-Length': postData.length
    }
  };
  
  var authenticate = helper.requestData(options, res, function(results) {
    config.access_token = results.access_token;
    res.render('login', { athlete : results });
  });

  authenticate.on('error', function(e) {
    console.log('problem with request: ' + e.message);
  });

  // write data to request body
  authenticate.write(postData);
  authenticate.end();
};


exports.logout = function(req, res) {

  var authHeader = { 'Authorization' : 'Bearer ' + config.access_token };

  var options = {
    hostname: 'www.strava.com',
    path: '/oauth/deauthorize',
    method: 'POST',
    headers: authHeader
  };

  var authenticate = helper.requestData(options, res, function(results) {
    // res.render('login', { athlete : results });
    res.redirect('/');
    console.log('Logged Out');
  });

  authenticate.on('error', function(e) {
    console.log('problem with request: ' + e.message);
  });
};